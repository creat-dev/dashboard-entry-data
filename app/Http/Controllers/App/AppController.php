<?php

namespace App\Http\Controllers\App;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use App\Models\Users;

class AppController extends Controller
{
    public function runApp(Request $request)
    {
        if($request->session()->has('User.ID')) {
            $user = Users::find($request->session()->get('User.ID'));
            $user->CurrentSign = $user->currentSignLog($request->session()->get('User.SignLogID'));
        }
        else {
            $user = false;
        }

        $view = view('app');
        $view->User = $user;
        return $view;
    }
}
