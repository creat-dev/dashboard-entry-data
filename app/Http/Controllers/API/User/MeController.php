<?php

namespace App\Http\Controllers\API\User;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Carbon\Carbon;

use App\Services\DataLogsServices as DataLogsServices;

use App\Models\Users;
use App\Models\ActivitiesLogs;

class MeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware(function ($request, $next) {
            $this->User = Users::find($request->session()->get('User.ID'));
            $this->User->CurrentSign = $this->User->currentSignLog($request->session()->get('User.SignLogID'));

            return $next($request);
        });
    }

    public function getMyDetails(Request $request)
    {
        if ($this->User) {
            $response = [
                'Status' => 'Success',
                'StatusCode' => '200#1',
                'StatusMsg' => 'Get me Details ♥',
                'User' => $this->User
            ];
            return response(json_encode($response), 200);
        }
    }

    public function updateDetails(Request $request)
    {
        $dateNow = Carbon::now();

        $user = Users::find($this->User->ID);
        if ($user) {
            // Create data log before update
            $log = DataLogsServices::tableUsers($user, [
                'CreatorIP' => $request->ip(),
                'CreatorAgent' => $request->header('User-Agent'),
                'CreatorSignLogID' => $this->User->CurrentSign->ID,
                'CreatorID' => $this->User->ID
            ]);
            
            $user->Username = $request->username;
            $user->Email = $request->email;
            $user->FullName = $request->fullname;
            $user->Phone = $request->phone;
            if ($request->has('password')) {
                $user->Password = Hash::make($request->password);
            }
            $user->UpdateAt = $dateNow;
            $user->save();

            // Create New Activity
            $activity = new ActivitiesLogs;
            $activity->Section = "me";
            $activity->Path = "me,edit";
            $activity->Type = "update";
            $activity->Action = "update";
            $activity->Data = $user->ID;
            $activity->CreatedAt = $dateNow;
            $activity->ActionByIP = $request->ip();
            $activity->ActionByAgent = $request->header('User-Agent');
            $activity->SignLogID = $this->User->CurrentSign->ID;
            $activity->ActionByID = $this->User->ID;
            $activity->save();

            $response = [
                'Status' => 'Success',
                'StatusCode' => '200#1',
                'StatusMsg' => 'Update my details ♥',
                'User' => $user
            ];
            return response(json_encode($response), 200);
        }
    }
}
