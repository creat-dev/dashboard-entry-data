<?php

namespace App\Http\Controllers\API\User;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Carbon\Carbon;

use App\Models\Users;
use App\Models\SignLogs;
use App\Models\ActivitiesLogs;

class AuthController extends Controller
{
    public function authSignIn(Request $request)
    {
        $dateNow = Carbon::now();

        $user = Users::where('Username', $request->username)->orWhere('Email', $request->username)->first();
        if ($user) {
            if (Hash::check($request->password, $user->Password)) {
                $user->LastSignInAt = $user->SignInAt;
                $user->SignInAt = $dateNow;
                $user->save();

                $sign = new SignLogs;
                $sign->IP = $request->ip();
                $sign->Country = strtolower(geoip($request->ip())['isoCode']);
                $sign->Agent = $request->header('User-Agent');
                $sign->SignInAt = $dateNow;
                $sign->SignOutAt = $dateNow;
                $sign->CreatedAt = $dateNow;
                $sign->UserID = $user->ID;
                $sign->save();

                // Create New Activity
                $activity = new ActivitiesLogs;
                $activity->Section = "me";
                $activity->Path = "me,auth";
                $activity->Type = "auth";
                $activity->Action = "sign-in";
                $activity->Data = $user->ID;
                $activity->CreatedAt = $dateNow;
                $activity->ActionByIP = $request->ip();
                $activity->ActionByAgent = $request->header('User-Agent');
                $activity->SignLogID = $sign->ID;
                $activity->ActionByID = $user->ID;
                $activity->save();

                $request->session()->put('User.ID', $user->ID);
                $request->session()->put('User.SignLogID', $sign->ID);

                $response = [
                    'Status' => 'Success',
                    'StatusCode' => '200#1',
                    'StatusMsg' => 'Good! Your Welcome ♥',
                ];
                return response(json_encode($response), 200);
            }
            else {
                $response = [
                    'Status' => 'Error',
                    'StatusCode' => '200#3',
                    'StatusMsg' => 'Incorrect Password',
                ];
                return response(json_encode($response), 200);
            }
        }
        else {
            $response = [
                'Status' => 'Error',
                'StatusCode' => '200#2',
                'StatusMsg' => 'Incorrect Username',
            ];

            return response(json_encode($response), 200);
        }
    }

    public function authSignOut(Request $request)
    {
        $request->session()->flush();

        $response = [
            'Status' => 'Success',
            'StatusCode' => '200#1',
            'StatusMsg' => 'Good Bye ♥',
        ];
        return response(json_encode($response), 200);
    }
}
