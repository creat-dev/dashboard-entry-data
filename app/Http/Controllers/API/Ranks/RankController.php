<?php

namespace App\Http\Controllers\API\Ranks;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Carbon\Carbon;

use App\Services\DataLogsServices as DataLogsServices;

use App\Models\Users;
use App\Models\Ranks;
use App\Models\ActivitiesLogs;

class RankController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware(function ($request, $next) {
            $this->User = Users::find($request->session()->get('User.ID'));
            $this->User->CurrentSign = $this->User->currentSignLog($request->session()->get('User.SignLogID'));

            return $next($request);
        });
    }

    public function getRank(Request $request, $rankID)
    {
        $rank = Ranks::find($rankID);
        if ($rank) {
            $response = [
                'Status' => 'Success',
                'StatusCode' => '200#1',
                'StatusMsg' => 'Get rank ♥',
                'Rank' => $rank
            ];
            return response(json_encode($response), 200);
        } else {
            $response = [
                'Status' => 'Error',
                'StatusCode' => '200#2',
                'StatusMsg' => 'Not found rank',
            ];
            return response(json_encode($response), 200);
        }
    }

    public function createRank(Request $request)
    {
        $dateNow = Carbon::now();

        $checkRank = Ranks::where('Title', $request->title)->first();
        if (!$checkRank) {
            $rank = new Ranks;
            $rank->Title = $request->title;
            $rank->CreatedAt = $dateNow;
            $rank->save();

            // Create New Activity
            $activity = new ActivitiesLogs;
            $activity->Section = "ranks";
            $activity->Path = "ranks,create";
            $activity->Type = "create";
            $activity->Action = "create";
            $activity->Data = $rank->ID;
            $activity->CreatedAt = $dateNow;
            $activity->ActionByIP = $request->ip();
            $activity->ActionByAgent = $request->header('User-Agent');
            $activity->SignLogID = $this->User->CurrentSign->ID;
            $activity->ActionByID = $this->User->ID;
            $activity->save();

            $response = [
                'Status' => 'Success',
                'StatusCode' => '200#1',
                'StatusMsg' => 'Create rank ♥',
                'Rank' => $rank
            ];
            return response(json_encode($response), 200);
        } else {
            $response = [
                'Status' => 'Error',
                'StatusCode' => '200#2',
                'StatusMsg' => 'Existing rank',
            ];
            return response(json_encode($response), 200);
        }
    }

    public function updateRank(Request $request, $rankID)
    {
        $dateNow = Carbon::now();

        $checkRank = Ranks::where('Title', $request->title)->where('ID', '!=', $rankID)->first();
        if (!$checkRank) {
            $rank = Ranks::find($rankID);

            // Create data log before update
            $log = DataLogsServices::tableRanks($rank, [
                'CreatorIP' => $request->ip(),
                'CreatorAgent' => $request->header('User-Agent'),
                'CreatorSignLogID' => $this->User->CurrentSign->ID,
                'CreatorID' => $this->User->ID
            ]);
            
            $rank->Title = $request->title;
            $rank->UpdateAt = $dateNow;
            $rank->save();

            // Create New Activity
            $activity = new ActivitiesLogs;
            $activity->Section = "ranks";
            $activity->Path = "ranks,rank,edit";
            $activity->Type = "update";
            $activity->Action = "update";
            $activity->Data = $rank->ID;
            $activity->CreatedAt = $dateNow;
            $activity->ActionByIP = $request->ip();
            $activity->ActionByAgent = $request->header('User-Agent');
            $activity->SignLogID = $this->User->CurrentSign->ID;
            $activity->ActionByID = $this->User->ID;
            $activity->save();

            $response = [
                'Status' => 'Success',
                'StatusCode' => '200#1',
                'StatusMsg' => 'Create rank ♥',
                'Rank' => $rank
            ];
            return response(json_encode($response), 200);
        } else {
            $response = [
                'Status' => 'Error',
                'StatusCode' => '200#2',
                'StatusMsg' => 'Existing rank',
            ];
            return response(json_encode($response), 200);
        }
    }
}
