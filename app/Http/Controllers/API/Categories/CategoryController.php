<?php

namespace App\Http\Controllers\API\Categories;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Carbon\Carbon;

use App\Services\DataLogsServices as DataLogsServices;

use App\Models\Users;
use App\Models\Categories;
use App\Models\ActivitiesLogs;

class CategoryController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware(function ($request, $next) {
            $this->User = Users::find($request->session()->get('User.ID'));
            $this->User->CurrentSign = $this->User->currentSignLog($request->session()->get('User.SignLogID'));

            return $next($request);
        });
    }

    public function getCategory(Request $request, $categoryID)
    {
        $category = Categories::find($categoryID);
        if ($category) {
            $response = [
                'Status' => 'Success',
                'StatusCode' => '200#1',
                'StatusMsg' => 'Get category ♥',
                'Category' => $category
            ];
            return response(json_encode($response), 200);
        } else {
            $response = [
                'Status' => 'Error',
                'StatusCode' => '200#2',
                'StatusMsg' => 'Not found category',
            ];
            return response(json_encode($response), 200);
        }
    }

    public function createCategory(Request $request)
    {
        $dateNow = Carbon::now();

        $checkCategory = Categories::where('Title', $request->title)->first();
        if (!$checkCategory) {
            $category = new Categories;
            $category->Title = $request->title;
            $category->CreatedAt = $dateNow;
            $category->save();

            // Create New Activity
            $activity = new ActivitiesLogs;
            $activity->Section = "categories";
            $activity->Path = "categories,create";
            $activity->Type = "create";
            $activity->Action = "create";
            $activity->Data = $category->ID;
            $activity->CreatedAt = $dateNow;
            $activity->ActionByIP = $request->ip();
            $activity->ActionByAgent = $request->header('User-Agent');
            $activity->SignLogID = $this->User->CurrentSign->ID;
            $activity->ActionByID = $this->User->ID;
            $activity->save();

            $response = [
                'Status' => 'Success',
                'StatusCode' => '200#1',
                'StatusMsg' => 'Create category ♥',
                'Category' => $category
            ];
            return response(json_encode($response), 200);
        } else {
            $response = [
                'Status' => 'Error',
                'StatusCode' => '200#2',
                'StatusMsg' => 'Existing category',
            ];
            return response(json_encode($response), 200);
        }
    }

    public function updateCategory(Request $request, $categoryID)
    {
        $dateNow = Carbon::now();

        $checkCategory = Categories::where('Title', $request->title)->where('ID', '!=', $categoryID)->first();
        if (!$checkCategory) {
            $category = Categories::find($categoryID);

            // Create data log before update
            $log = DataLogsServices::tableCategories($category, [
                'CreatorIP' => $request->ip(),
                'CreatorAgent' => $request->header('User-Agent'),
                'CreatorSignLogID' => $this->User->CurrentSign->ID,
                'CreatorID' => $this->User->ID
            ]);
            
            $category->Title = $request->title;
            $category->UpdateAt = $dateNow;
            $category->save();

            // Create New Activity
            $activity = new ActivitiesLogs;
            $activity->Section = "categories";
            $activity->Path = "categories,category,edit";
            $activity->Type = "update";
            $activity->Action = "update";
            $activity->Data = $category->ID;
            $activity->CreatedAt = $dateNow;
            $activity->ActionByIP = $request->ip();
            $activity->ActionByAgent = $request->header('User-Agent');
            $activity->SignLogID = $this->User->CurrentSign->ID;
            $activity->ActionByID = $this->User->ID;
            $activity->save();

            $response = [
                'Status' => 'Success',
                'StatusCode' => '200#1',
                'StatusMsg' => 'Create category ♥',
                'Category' => $category
            ];
            return response(json_encode($response), 200);
        } else {
            $response = [
                'Status' => 'Error',
                'StatusCode' => '200#2',
                'StatusMsg' => 'Existing category',
            ];
            return response(json_encode($response), 200);
        }
    }
}
