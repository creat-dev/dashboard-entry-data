<?php

namespace App\Http\Controllers\API\Units;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Carbon\Carbon;

use App\Services\DataLogsServices as DataLogsServices;

use App\Models\Users;
use App\Models\Units;
use App\Models\ActivitiesLogs;

class UnitController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware(function ($request, $next) {
            $this->User = Users::find($request->session()->get('User.ID'));
            $this->User->CurrentSign = $this->User->currentSignLog($request->session()->get('User.SignLogID'));

            return $next($request);
        });
    }

    public function getUnit(Request $request, $unitID)
    {
        $unit = Units::find($unitID);
        if ($unit) {
            $response = [
                'Status' => 'Success',
                'StatusCode' => '200#1',
                'StatusMsg' => 'Get unit ♥',
                'Unit' => $unit
            ];
            return response(json_encode($response), 200);
        } else {
            $response = [
                'Status' => 'Error',
                'StatusCode' => '200#2',
                'StatusMsg' => 'Not found unit',
            ];
            return response(json_encode($response), 200);
        }
    }

    public function createUnit(Request $request)
    {
        $dateNow = Carbon::now();

        $checkUnit = Units::where('Title', $request->title)->first();
        if (!$checkUnit) {
            $unit = new Units;
            $unit->Title = $request->title;
            $unit->CreatedAt = $dateNow;
            $unit->save();

            // Create New Activity
            $activity = new ActivitiesLogs;
            $activity->Section = "units";
            $activity->Path = "units,create";
            $activity->Type = "create";
            $activity->Action = "create";
            $activity->Data = $unit->ID;
            $activity->CreatedAt = $dateNow;
            $activity->ActionByIP = $request->ip();
            $activity->ActionByAgent = $request->header('User-Agent');
            $activity->SignLogID = $this->User->CurrentSign->ID;
            $activity->ActionByID = $this->User->ID;
            $activity->save();

            $response = [
                'Status' => 'Success',
                'StatusCode' => '200#1',
                'StatusMsg' => 'Create unit ♥',
                'Unit' => $unit
            ];
            return response(json_encode($response), 200);
        } else {
            $response = [
                'Status' => 'Error',
                'StatusCode' => '200#2',
                'StatusMsg' => 'Existing unit',
            ];
            return response(json_encode($response), 200);
        }
    }

    public function updateUnit(Request $request, $unitID)
    {
        $dateNow = Carbon::now();

        $checkUnit = Units::where('Title', $request->title)->where('ID', '!=', $unitID)->first();
        if (!$checkUnit) {
            $unit = Units::find($unitID);

            // Create data log before update
            $log = DataLogsServices::tableUnits($unit, [
                'CreatorIP' => $request->ip(),
                'CreatorAgent' => $request->header('User-Agent'),
                'CreatorSignLogID' => $this->User->CurrentSign->ID,
                'CreatorID' => $this->User->ID
            ]);
            
            $unit->Title = $request->title;
            $unit->UpdateAt = $dateNow;
            $unit->save();

            // Create New Activity
            $activity = new ActivitiesLogs;
            $activity->Section = "units";
            $activity->Path = "units,unit,edit";
            $activity->Type = "update";
            $activity->Action = "update";
            $activity->Data = $unit->ID;
            $activity->CreatedAt = $dateNow;
            $activity->ActionByIP = $request->ip();
            $activity->ActionByAgent = $request->header('User-Agent');
            $activity->SignLogID = $this->User->CurrentSign->ID;
            $activity->ActionByID = $this->User->ID;
            $activity->save();

            $response = [
                'Status' => 'Success',
                'StatusCode' => '200#1',
                'StatusMsg' => 'Create unit ♥',
                'Unit' => $unit
            ];
            return response(json_encode($response), 200);
        } else {
            $response = [
                'Status' => 'Error',
                'StatusCode' => '200#2',
                'StatusMsg' => 'Existing unit',
            ];
            return response(json_encode($response), 200);
        }
    }
}
