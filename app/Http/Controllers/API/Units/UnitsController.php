<?php

namespace App\Http\Controllers\API\Units;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use App\Models\Users;
use App\Models\Units;

class UnitsController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware(function ($request, $next) {
            $this->User = Users::find($request->session()->get('User.ID'));
            $this->User->CurrentSign = $this->User->currentSignLog($request->session()->get('User.SignLogID'));

            return $next($request);
        });
    }

    public function getAll()
    {
        $units = Units::all();

        $response = [
            'Status' => 'Success',
            'StatusCode' => '200#1',
            'StatusMsg' => 'Good! Get Units ♥',
            'Units' => $units
        ];
        return response(json_encode($response), 200);
    }
}
