<?php

namespace App\Http\Controllers\API\Dashboard;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use App\Models\Users;
use App\Models\Records;
use App\Models\Ranks;
use App\Models\Categories;
use App\Models\Units;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware(function ($request, $next) {
            $this->User = Users::find($request->session()->get('User.ID'));
            $this->User->CurrentSign = $this->User->currentSignLog($request->session()->get('User.SignLogID'));

            return $next($request);
        });
    }

    public function getStatistics()
    {
        $records = Records::count();
        $ranks = Ranks::count();
        $categories = Categories::count();
        $units = Units::count();

        $response = [
            'Status' => 'Success',
            'StatusCode' => '200#1',
            'StatusMsg' => 'Good! Get Statistics ♥',
            'Statistics' => [
                'Records' => $records,
                'Ranks' => $ranks,
                'Categories' => $categories,
                'Units' => $units
            ]
        ];
        return response(json_encode($response), 200);
    }
}
