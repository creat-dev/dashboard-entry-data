<?php

namespace App\Http\Controllers\API\Records;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Carbon\Carbon;
use Jenssegers\Date\Date;

use App\Services\DataLogsServices as DataLogsServices;

use App\Models\Users;
use App\Models\Records;
use App\Models\ActivitiesLogs;

class RecordController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware(function ($request, $next) {
            $this->User = Users::find($request->session()->get('User.ID'));
            $this->User->CurrentSign = $this->User->currentSignLog($request->session()->get('User.SignLogID'));

            return $next($request);
        });
    }

    public function getRecord(Request $request, $recordID)
    {
        $record = Records::find($recordID);
        if ($record) {
            $response = [
                'Status' => 'Success',
                'StatusCode' => '200#1',
                'StatusMsg' => 'Get record ♥',
                'Record' => $record
            ];
            return response(json_encode($response), 200);
        } else {
            $response = [
                'Status' => 'Error',
                'StatusCode' => '200#2',
                'StatusMsg' => 'Not found record',
            ];
            return response(json_encode($response), 200);
        }
    }

    public function getRecordWithDetails(Request $request, $recordID)
    {
        $record = Records::find($recordID);
        $record->Rank = $record->rank;
        $record->Unit = $record->unit;
        $record->Category = $record->category;
        $record->CreatedAt = Date::parse($record->CreatedAt)->format("l، d F Y / h:i:s A");
        $record->UpdateAt = !is_null($record->UpdateAt) ? Date::parse($record->UpdateAt)->format("l، d F Y / h:i:s A") : $record->CreatedAt;
        if ($record) {
            $response = [
                'Status' => 'Success',
                'StatusCode' => '200#1',
                'StatusMsg' => 'Get record ♥',
                'Record' => $record
            ];
            return response(json_encode($response), 200);
        } else {
            $response = [
                'Status' => 'Error',
                'StatusCode' => '200#2',
                'StatusMsg' => 'Not found record',
            ];
            return response(json_encode($response), 200);
        }
    }

    public function createRecord(Request $request)
    {
        $dateNow = Carbon::now();

        $checkRecord = Records::where('Title', $request->title)->first();
        if (!$checkRecord) {
            $record = new Records;
            $record->Title = $request->title;
            $record->Value = $request->value;
            $record->UnitID = $request->unit;
            $record->CategoryID = $request->category;
            $record->RankID = $request->rank;
            $record->CreatedAt = $dateNow;
            $record->save();

            // Create New Activity
            $activity = new ActivitiesLogs;
            $activity->Section = "records";
            $activity->Path = "records,create";
            $activity->Type = "create";
            $activity->Action = "create";
            $activity->Data = $record->ID;
            $activity->CreatedAt = $dateNow;
            $activity->ActionByIP = $request->ip();
            $activity->ActionByAgent = $request->header('User-Agent');
            $activity->SignLogID = $this->User->CurrentSign->ID;
            $activity->ActionByID = $this->User->ID;
            $activity->save();

            $response = [
                'Status' => 'Success',
                'StatusCode' => '200#1',
                'StatusMsg' => 'Create record ♥',
                'Record' => $record
            ];
            return response(json_encode($response), 200);
        } else {
            $response = [
                'Status' => 'Error',
                'StatusCode' => '200#2',
                'StatusMsg' => 'Existing record',
            ];
            return response(json_encode($response), 200);
        }
    }

    public function updateRecord(Request $request, $recordID)
    {
        $dateNow = Carbon::now();

        $checkRecord = Records::where('Title', $request->title)->where('ID', '!=', $recordID)->first();
        if (!$checkRecord) {
            $record = Records::find($recordID);

            // Create data log before update
            $log = DataLogsServices::tableRecords($record, [
                'CreatorIP' => $request->ip(),
                'CreatorAgent' => $request->header('User-Agent'),
                'CreatorSignLogID' => $this->User->CurrentSign->ID,
                'CreatorID' => $this->User->ID
            ]);
            
            $record->Title = $request->title;
            $record->Value = $request->value;
            $record->UnitID = $request->unit;
            $record->CategoryID = $request->category;
            $record->RankID = $request->rank;
            $record->UpdateAt = $dateNow;
            $record->save();

            // Create New Activity
            $activity = new ActivitiesLogs;
            $activity->Section = "records";
            $activity->Path = "records,record,edit";
            $activity->Type = "update";
            $activity->Action = "update";
            $activity->Data = $record->ID;
            $activity->CreatedAt = $dateNow;
            $activity->ActionByIP = $request->ip();
            $activity->ActionByAgent = $request->header('User-Agent');
            $activity->SignLogID = $this->User->CurrentSign->ID;
            $activity->ActionByID = $this->User->ID;
            $activity->save();

            $response = [
                'Status' => 'Success',
                'StatusCode' => '200#1',
                'StatusMsg' => 'Create records ♥',
                'Record' => $record
            ];
            return response(json_encode($response), 200);
        } else {
            $response = [
                'Status' => 'Error',
                'StatusCode' => '200#2',
                'StatusMsg' => 'Existing records',
            ];
            return response(json_encode($response), 200);
        }
    }
}
