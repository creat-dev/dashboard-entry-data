<?php

namespace App\Http\Controllers\API\Records;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use App\Models\Users;
use App\Models\Records;
use App\Models\Ranks;
use App\Models\Categories;
use App\Models\Units;

class RecordsController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware(function ($request, $next) {
            $this->User = Users::find($request->session()->get('User.ID'));
            $this->User->CurrentSign = $this->User->currentSignLog($request->session()->get('User.SignLogID'));

            return $next($request);
        });
    }

    public function getAll()
    {
        $records = Records::all();
        foreach ($records as &$record) {
            $record->Rank = $record->rank;
            $record->Unit = $record->unit;
            $record->Category = $record->category;
        }

        $response = [
            'Status' => 'Success',
            'StatusCode' => '200#1',
            'StatusMsg' => 'Good! Get Records ♥',
            'Records' => $records
        ];
        return response(json_encode($response), 200);
    }
}
