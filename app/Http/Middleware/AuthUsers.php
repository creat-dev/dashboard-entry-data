<?php

namespace App\Http\Middleware;

use Closure;
use Session;
use Config;
use Carbon\Carbon;

use App\Models\SignLogs;

class AuthUsers
{
    /**
     * The names of the cookies that should not be encrypted.
     *
     * @var array
     */
    protected $except = [
        'sign-in',
        'api/user/auth/sign-in',
        'sign-out',
    ];



    /**
     * Determine if the request has a URI that should pass through CSRF verification.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return bool
     */
    protected function shouldPassThrough($request)
    {
        foreach ($this->except as $except) {
            if ($except !== '/') {
                $except = trim($except, '/');
            }

            if ($request->is($except)) {
                return true;
            }
        }

        return false;
    }

    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @param  string|null  $guard
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if (!$this->shouldPassThrough($request)) {
            if (!Session::has('User.ID') || !Session::has('User.SignLogID')) {
                return redirect('/sign-in')->cookie('return_url', $request->path());
            }
        }

        if (Session::has('User.ID') && Session::has('User.SignLogID')) {
            $sign = SignLogs::find(Session::get('User.SignLogID'));
            $sign->SignOutAt = Carbon::now();
            $sign->save();
        }

        return $next($request);
    }
}
