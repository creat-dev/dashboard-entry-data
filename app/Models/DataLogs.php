<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class DataLogs extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'data_logs';

    /**
	 * Primary key
	 *
	 * @var string
	 */
	protected $primaryKey = 'ID';

    /**
     * The column cancelled
     *
     * @var string
     */
    public $timestamps = false;
}
