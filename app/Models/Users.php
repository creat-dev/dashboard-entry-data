<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

use App\Models\SignLogs;

class Users extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'users';

    /**
	 * Primary key
	 *
	 * @var string
	 */
	protected $primaryKey = 'ID';

    /**
     * The column cancelled
     *
     * @var string
     */
    public $timestamps = false;

    /**
     * The function for get sign in log
     *
     * @param signLogID int
     */
    public function currentSignLog($signLogID)
    {
        return SignLogs::find($signLogID);
    }
}
