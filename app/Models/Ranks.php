<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Ranks extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'ranks';

    /**
	 * Primary key
	 *
	 * @var string
	 */
	protected $primaryKey = 'ID';

    /**
     * The column cancelled
     *
     * @var string
     */
    public $timestamps = false;
}
