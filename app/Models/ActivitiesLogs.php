<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ActivitiesLogs extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'activities_logs';

    /**
	 * Primary key
	 *
	 * @var string
	 */
	protected $primaryKey = 'ID';

    /**
     * The column cancelled
     *
     * @var string
     */
    public $timestamps = false;
}
