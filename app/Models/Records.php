<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Records extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'records';

    /**
	 * Primary key
	 *
	 * @var string
	 */
	protected $primaryKey = 'ID';

    /**
     * The column cancelled
     *
     * @var string
     */
    public $timestamps = false;

    /**
     * The function for get record rank
     */
    public function rank()
    {
        return $this->hasOne('App\Models\Ranks', 'ID', 'RankID');
    }

    /**
     * The function for get record unit
     */
    public function unit()
    {
        return $this->hasOne('App\Models\Units', 'ID', 'UnitID');
    }

    /**
     * The function for get record rank
     */
    public function category()
    {
        return $this->hasOne('App\Models\Categories', 'ID', 'CategoryID');
    }
}
