<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Categories extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'categories';

    /**
	 * Primary key
	 *
	 * @var string
	 */
	protected $primaryKey = 'ID';

    /**
     * The column cancelled
     *
     * @var string
     */
    public $timestamps = false;
}
