<?php

namespace App\Services;

use App\Models\DataLogs;
use Carbon\Carbon;

class DataLogsServices
{
    public static function tableRecords($record, $details)
    {
        $log = new \stdClass;
        $log->Table = "records";
        $log->ID = $record->ID;
        $log->Version = "1.0.0";
        $log->CreateByIP = $details['CreatorIP'];
        $log->CreateByAgent = $details['CreatorAgent'];
        $log->CreateBySignLogID = $details['CreatorSignLogID'];
        $log->CreateByID = $details['CreatorID'];

        return self::create($record, $log);
    }
    
    public static function tableCategories($record, $details)
    {
        $log = new \stdClass;
        $log->Table = "categories";
        $log->ID = $record->ID;
        $log->Version = "1.0.0";
        $log->CreateByIP = $details['CreatorIP'];
        $log->CreateByAgent = $details['CreatorAgent'];
        $log->CreateBySignLogID = $details['CreatorSignLogID'];
        $log->CreateByID = $details['CreatorID'];

        return self::create($record, $log);
    }
    
    public static function tableUnits($record, $details)
    {
        $log = new \stdClass;
        $log->Table = "units";
        $log->ID = $record->ID;
        $log->Version = "1.0.0";
        $log->CreateByIP = $details['CreatorIP'];
        $log->CreateByAgent = $details['CreatorAgent'];
        $log->CreateBySignLogID = $details['CreatorSignLogID'];
        $log->CreateByID = $details['CreatorID'];

        return self::create($record, $log);
    }
    
    public static function tableRanks($record, $details)
    {
        $log = new \stdClass;
        $log->Table = "ranks";
        $log->ID = $record->ID;
        $log->Version = "1.0.0";
        $log->CreateByIP = $details['CreatorIP'];
        $log->CreateByAgent = $details['CreatorAgent'];
        $log->CreateBySignLogID = $details['CreatorSignLogID'];
        $log->CreateByID = $details['CreatorID'];

        return self::create($record, $log);
    }
    
    public static function tableUsers($record, $details)
    {
        $log = new \stdClass;
        $log->Table = "users";
        $log->ID = $record->ID;
        $log->Version = "1.0.0";
        $log->CreateByIP = $details['CreatorIP'];
        $log->CreateByAgent = $details['CreatorAgent'];
        $log->CreateBySignLogID = $details['CreatorSignLogID'];
        $log->CreateByID = $details['CreatorID'];

        return self::create($record, $log);
    }

    public static function create($record, $details)
    {
        $log = new DataLogs;
        $log->TableName = $details->Table;
        $log->TableID = $details->ID;
        $log->Version = $details->Version;
        $log->Data = $record;
        $log->CreatedAt = Carbon::now();
        $log->CreateByIP = $details->CreateByIP;
        $log->CreateByAgent = $details->CreateByAgent;
        $log->SignLogID = $details->CreateBySignLogID;
        $log->CreateByID = $details->CreateByID;
        $log->save();

        return $log;
    }
}
