<!DOCTYPE html>
<html lang="{{ config('app.locale') }}">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        
        @if($User)
        <title>الهيئة العامة للإحصاء</title>
        @else
        <title>تسجيل الدخول</title>
        @endif

        <!-- Fonts -->
        <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.6.3/css/font-awesome.min.css" rel="stylesheet" type="text/css">
        {!! Html::style('/static/assets/css/fonts-ng4asans.css') !!}

        <!-- Styles -->
        {!! Html::style('/static/interface/css/app.min.css') !!}
    </head>
    <body>
        <main id="app">
            @if($User)
            <header id="header">
                <aside class="top">
                    <div class="flex-cntr flex-spacebetween cntr">
                        <div class="logo">
                            <img src="/static/interface/images/logo.png" alt="الهيئة العامة للاحصاء">
                        </div>
                        <div class="flex-cntr flex-spacebetween account">
                            <router-link to="/me">
                                <i class="fa fa-user"></i>
                                <span id="userFullName">{{ $User->FullName }}</span>
                            </router-link>
                            <router-link to="/sign-out">
                                <i class="fa fa-sign-out"></i> خروج
                            </router-link>
                        </div>
                    </div>
                </aside>
                <aside class="bottom">
                    <div class="cntr">
                        <nav class="flex-cntr flex-start menu">
                            <ul class="flex-cntr flex-start links">
                                <li>
                                    <router-link to="/dashboard">
                                        <i class="fa fa-dashboard"></i> لوحة التحكم
                                    </router-link>
                                </li>
                                <li>
                                    <a href="javascript:;" class="dropmenu">
                                        <i class="fa fa-line-chart"></i>
                                        <span>الاحصائيات</span>
                                        <i class="fa fa-chevron-down"></i>
                                    </a>
                                    <ul class="submenu">
                                        <li>
                                            <router-link to="/records/view">عرض الكل</router-link>
                                        </li>
                                        <li>
                                            <router-link to="/records/create">إضافة بيانات</router-link>
                                        </li>
                                    </ul>
                                </li>
                                <li>
                                    <a href="javascript:;" class="dropmenu">
                                        <i class="fa fa-tags"></i>
                                        <span>الفئات</span>
                                        <i class="fa fa-chevron-down"></i>
                                    </a>
                                    <ul class="submenu">
                                        <li>
                                            <router-link to="/categories/view">عرض الكل</router-link>
                                        </li>
                                        <li>
                                            <router-link to="/categories/create">إضافة فئة</router-link>
                                        </li>
                                    </ul>
                                </li>
                                <li>
                                    <a href="javascript:;" class="dropmenu">
                                        <i class="fa fa-cubes"></i>
                                        <span>الوحدات</span>
                                        <i class="fa fa-chevron-down"></i>
                                    </a>
                                    <ul class="submenu">
                                        <li>
                                            <router-link to="/units/view">عرض الكل</router-link>
                                        </li>
                                        <li>
                                            <router-link to="/units/create">إضافة وحدة</router-link>
                                        </li>
                                    </ul>
                                </li>
                                <li>
                                    <a href="javascript:;" class="dropmenu">
                                        <i class="fa fa-star"></i>
                                        <span>الرتب</span>
                                        <i class="fa fa-chevron-down"></i>
                                    </a>
                                    <ul class="submenu">
                                        <li>
                                            <router-link to="/ranks/view">عرض الكل</router-link>
                                        </li>
                                        <li>
                                            <router-link to="/ranks/create">إضافة الرتب</router-link>
                                        </li>
                                    </ul>
                                </li>
                            </ul>
                        </nav>
                        <div class="flex-cntr flex-start serach">
                        </div>
                    </div>
                </aside>
            </header>
            <section id="content">
                <transition v-on:before-enter="beforeEnter" v-on:after-enter="afterEnter">
                    <router-view class="loadingActiveCntr"></router-view>
                </transition>
                <div id="LoadingContent">
                    <img src="/static/interface/images/cube-5663ab.svg">
                </div>
            </section>
            @else
            <section id="content" class="withoutProp">
                <transition v-on:before-enter="beforeEnter" v-on:after-enter="afterEnter">
                    <router-view transition="PageLoading"></router-view>
                </transition>
            </section>
            @endif
            <div id="LoadingPage">
                <img src="/static/interface/images/cube-white.svg">
            </div>
        </main>
        <script>window.Laravel = <?php echo json_encode(['csrfToken' => csrf_token()]); ?>;</script>
        {!! Html::script('/static/interface/js/core.min.js') !!}
        {!! Html::script('/static/interface/js/app.min.js') !!}
    </body>
</html>
