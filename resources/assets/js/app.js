Vue.filter('firstZero', function (value, numZero) {
    function pad (str) {
        if (str) {
            if (!(typeof str === 'string')) {
                str = str.toString();
            }
            return str.length < numZero ? pad("0" + str) : str;
        }
    }
    return pad(value);
});
Vue.filter('checkEmail', function (email) {
    var re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return re.test(email);
});
Vue.filter('checkEmpty', function (elements, elementAlert) {
    var isEmpty = false;
    elements.each(function() {
        var iselementEmpty = false;
        var element = $(this);
        if (element.val() == "") {
            isEmpty = true;
            iselementEmpty = true;
            element.addClass("error");
        }
        else {
            element.removeClass("error");
        }

        if (iselementEmpty == true) {
            elementAlert.fadeIn();
        }
        else {
            elementAlert.fadeOut();
        }
    });

    return isEmpty;
});

/* Routes */
const routes = [
  { path: '/', redirect: '/dashboard' },
  { path: '/sign-in', component: require('./components/auth/sign-in.vue') },
  { path: '/sign-out', component: require('./components/auth/sign-out.vue') },
  { path: '/me', component: require('./components/user/me.vue') },
  { path: '/dashboard', component: require('./components/dashboard/home.vue') },
  { path: '/records', redirect: '/records/view' },
  { path: '/records/view', component: require('./components/records/view/all.vue') },
  { path: '/records/create', component: require('./components/records/record/create.vue') },
  { path: '/records/record/:id', component: require('./components/records/record/view.vue'), props: true },
  { path: '/records/record/:id/edit', component: require('./components/records/record/edit.vue'), props: true },
  { path: '/categories', redirect: '/categories/view' },
  { path: '/categories/view', component: require('./components/categories/view/all.vue') },
  { path: '/categories/create', component: require('./components/categories/category/create.vue') },
  { path: '/categories/category/:id/edit', component: require('./components/categories/category/edit.vue'), props: true },
  { path: '/units', redirect: '/units/view' },
  { path: '/units/view', component: require('./components/units/view/all.vue') },
  { path: '/units/create', component: require('./components/units/unit/create.vue') },
  { path: '/units/unit/:id/edit', component: require('./components/units/unit/edit.vue'), props: true },
  { path: '/ranks', redirect: '/ranks/view' },
  { path: '/ranks/view', component: require('./components/ranks/view/all.vue') },
  { path: '/ranks/create', component: require('./components/ranks/rank/create.vue') },
  { path: '/ranks/rank/:id/edit', component: require('./components/ranks/rank/edit.vue'), props: true },
  { path: '*', component: require('./components/errors/404.vue') }
];

/**
 * routes option
 */
const router = new VueRouter({
    mode: 'history',
    linkActiveClass: 'active',
    routes
})

/* Run app */
const app = new Vue({
    router,
    methods: {
        beforeEnter: function () {
            $("#app #content .loadingActiveCntr").hide();
            $("#LoadingContent").css('display', 'flex');
            $("#LoadingContent").animate({ opacity: 1 }, 500);
        },
        afterEnter: function () {
            $("#LoadingContent").animate({ opacity: 0 }, 500, function() {
                $("#LoadingContent").hide();
                $("#app #content .loadingActiveCntr").fadeIn();
            });
        }
    }
}).$mount('#app')

// After run app
$("#app #content, #app #header").hide()
$("#LoadingContent").hide();
$("#LoadingPage").animate({ opacity: 0 }, 1000, function() {
    $("#LoadingPage").hide();
    $("#app #content, #app #header").fadeIn()
});

/**
 * jQuery Function & Methods
 */
$("html, body").click(function() {
    $("#header .menu .dropmenu").not(".open").removeClass("active");
    $("#header .menu .dropmenu").next(".submenu").slideUp();
});

$("#header .menu .dropmenu").click(function(e) {
    e.preventDefault();
    e.stopPropagation();
    $("#header .menu .dropmenu").not(this).not(".open").removeClass("active");
    $("#header .menu .dropmenu").not(this).next(".submenu").slideUp();
    $(this).not(".open").toggleClass("active");
    $(this).next(".submenu").slideToggle();
});

$("#header .OpenBoxSearch").click(function() {
    $("#header .search").slideToggle();
});