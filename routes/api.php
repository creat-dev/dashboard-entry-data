<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::group(['prefix' => 'user', 'namespace' => 'User'], function() {
    Route::post('/auth/sign-in', 'AuthController@authSignIn');
    Route::post('/auth/sign-out', 'AuthController@authSignOut');
    Route::post('/me', 'MeController@getMyDetails');
    Route::post('/me/update', 'MeController@updateDetails');
});

Route::group(['prefix' => 'dashboard', 'namespace' => 'Dashboard'], function() {
    Route::post('/statistics', 'HomeController@getStatistics');
});

Route::group(['prefix' => 'records', 'namespace' => 'Records'], function() {
    Route::post('/all', 'RecordsController@getAll');

    Route::group(['prefix' => 'record'], function() {
        Route::post('/create', 'RecordController@createRecord');

        Route::group(['prefix' => '{record_id}'], function() {
            Route::post('/', 'RecordController@getRecord');
            Route::post('/with-details', 'RecordController@getRecordWithDetails');
            Route::post('/update', 'RecordController@updateRecord');
        });
    });
});

Route::group(['prefix' => 'categories', 'namespace' => 'Categories'], function() {
    Route::post('/all', 'CategoriesController@getAll');

    Route::group(['prefix' => 'category'], function() {
        Route::post('/create', 'CategoryController@createCategory');

        Route::group(['prefix' => '{category_id}'], function() {
            Route::post('/', 'CategoryController@getCategory');
            Route::post('/update', 'CategoryController@updateCategory');
        });
    });
});

Route::group(['prefix' => 'units', 'namespace' => 'Units'], function() {
    Route::post('/all', 'UnitsController@getAll');

    Route::group(['prefix' => 'unit'], function() {
        Route::post('/create', 'UnitController@createUnit');

        Route::group(['prefix' => '{unit_id}'], function() {
            Route::post('/', 'UnitController@getUnit');
            Route::post('/update', 'UnitController@updateUnit');
        });
    });
});

Route::group(['prefix' => 'ranks', 'namespace' => 'Ranks'], function() {
    Route::post('/all', 'RanksController@getAll');

    Route::group(['prefix' => 'rank'], function() {
        Route::post('/create', 'RankController@createRank');

        Route::group(['prefix' => '{rank_id}'], function() {
            Route::post('/', 'RankController@getRank');
            Route::post('/update', 'RankController@updateRank');
        });
    });
});