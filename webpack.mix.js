const { mix } = require('laravel-mix');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */

mix.js('resources/assets/js/bootstrap.js', 'public/static/interface/js/core.min.js');
mix.js('resources/assets/js/app.js', 'public/static/interface/js/app.min.js');
mix.combine(['public/static/interface/css/main.css'], 'public/static/interface/css/app.min.css');
